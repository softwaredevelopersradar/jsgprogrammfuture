package FPSModels;

public class FPSModel {
//таблица состояния
    private int power;
    private float ksv;
    private boolean supply;
    private boolean radiation;
    private boolean filter;
    private int temperature;
    private boolean control;

    public FPSModel() {

    }

    public FPSModel(int power, float ksv, boolean supply, boolean radiation, boolean filter, int temperature, boolean control) {

        this.power = power;
        this.ksv = ksv;
        this.supply = false;
        this.radiation = false;
        this.filter = false;
        this.temperature = temperature;
        this.control = false;

    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Float getKsv() {
        return ksv;
    }

    public void setKsv(Float ksv) {
        this.ksv = ksv;
    }

    public Boolean getSupply() {
        return supply;
    }

    public void setSupply(Boolean supply) {
        this.supply = supply;
    }

    public Boolean getRadiation() {
        return radiation;
    }

    public void setRadiation(Boolean radiation) {
        this.radiation = radiation;
    }

    public Boolean getFilter() {
        return filter;
    }

    public void setFilter(Boolean filter) {
        this.filter = filter;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Boolean getControl() {
        return control;
    }

    public void setControl(Boolean control) {
        this.control = control;
    }

}
