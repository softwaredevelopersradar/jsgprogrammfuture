package FPSModels;

public enum AnalogSignalType {
    AM((byte) 0, "AM"),
    FM((byte) 1, "FM"),
    USB((byte) 2, "USB"),
    LSB((byte) 3, "LSB");

    private byte type;
    private String name;

    AnalogSignalType(byte i, String n) {
        type = i;
        name = n;
    }

    public byte getAnalogSignalType() {
        return type;
    }

    public String getAnalogSignalTypeName(){
        return name;
    }
}
