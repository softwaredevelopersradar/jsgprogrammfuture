package FPSModels;

public class ButtonIndicatorsModel {
    private Led LedOnOffPower;
    private Led LedErrorPower;
    private Led LedFilter1;
    private Led LedFilter2;
    private Led LedTS1;
    private Led LedTS2;
    private Led LedTS3;
    private Led LedRadiationOnOff;
    private Led LedErrorRadiation;

    public ButtonIndicatorsModel() {
        this.LedFilter1 = Led.Empty;
        this.LedErrorPower = Led.Empty;
        this.LedFilter2 = Led.Empty;
        this.LedOnOffPower = Led.Empty;
        this.LedTS1 = Led.Empty;
        this.LedTS2 = Led.Empty;
        this.LedTS3 = Led.Empty;
        this.LedRadiationOnOff = Led.Empty;
        this.LedErrorRadiation = Led.Empty;
    }

    public ButtonIndicatorsModel(Led ledFilter1, Led ledErrorPower, Led ledFilter2, Led ledOnOffPower, Led ledTS1, Led ledTS2, Led ledTS3, Led ledRadiationOnOff, Led ledErrorRadiation){
        this.LedOnOffPower = ledOnOffPower;
        this.LedErrorPower = ledErrorPower;
        this.LedFilter1 = ledFilter1;
        this.LedFilter2 = ledFilter2;
        this.LedTS1 = ledTS1;
        this.LedTS2 = ledTS2;
        this.LedTS3 = ledTS3;
        this.LedRadiationOnOff = ledRadiationOnOff;
        this.LedErrorRadiation = ledErrorRadiation;
    }
    public Led getLedOnOffPower() {
        return LedOnOffPower;
    }
    public void setLedOnOffPower(Led ledOnOffPower) { this.LedOnOffPower = ledOnOffPower; }

    public Led getLedErrorPower() {
        return LedErrorPower;
    }
    public void setLedErrorPower(Led ledErrorPower) { this.LedErrorPower = ledErrorPower; }

    public Led getLedFilter1() {
        return LedFilter1;
    }
    public void setLedFilter1(Led ledFilter1) { this.LedFilter1 = ledFilter1; }

    public Led getLedFilter2() {
        return LedFilter2;
    }
    public void setLedFilter2(Led ledFilter2) { this.LedFilter2 = ledFilter2; }

    public Led getLedTS1() {
        return LedTS1;
    }
    public void setLedTS1(Led ledTS1) { this.LedTS1 = ledTS1; }

    public Led getLedTS2() {
        return LedTS2;
    }
    public void setLedTS2(Led ledTS2) { this.LedTS2 = ledTS2; }

    public Led getLedTS3() {
        return LedTS3;
    }
    public void setLedTS3(Led ledTS3) { this.LedTS3 = ledTS3; }

    public Led getLedRadiationOnOff() {
        return LedRadiationOnOff;
    }
    public void setLedRadiationOnOff(Led ledRadiationOnOff) { this.LedRadiationOnOff = ledRadiationOnOff; }

    public Led getLedErrorRadiation() {
        return LedErrorRadiation;
    }
    public void setLedErrorRadiation(Led ledErrorRadiation) { this.LedErrorRadiation = ledErrorRadiation; }

}

