package FPSModels;

public enum Led {
    Empty((byte)0),
    Green((byte)1),
    Red((byte)2),
    Blue((byte)3),
    Yellow((byte)4),
    Gray((byte)5);
    private Led(byte b) {

    }
    public Byte getByte() {
        switch(this) {
            case Empty:
                return 0;
            case Green:
                return 1;
            case Red:
                return 2;
            case Blue:
                return 3;
            case Yellow:
                return 4;
            case Gray:
                return 5;
            default:
                return 0;
        }
    }

    public static Led getLed(Byte b) {
        switch(b) {
            case 0:
                return Empty;
            case 1:
                return Green;
            case 2:
                return Red;
            case 3:
                return Blue;
            case 4:
                return Yellow;
            case 5:
                return Gray;
            default:
                return Empty;
        }
    }
}


