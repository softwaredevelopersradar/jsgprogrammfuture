package FPSModels;

import java.util.Date;

public class ErrorsModel {
    //таблица описаний
    private Byte description;
    private Date time = new Date();


    public ErrorsModel(byte description) {


        this.description = description;
        this.time = new Date(time.getTime());
    }



    public Byte getDescription() {
        return description;
    }
    public void setDescription(Byte description) {
        this.description = description;
    }
    public Date getTime() {
        return this.time;
    }
    public void setTime(Date time) {
        this.time = time;
    }
}




