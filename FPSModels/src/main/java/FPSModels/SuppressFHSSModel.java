package FPSModels;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


public class SuppressFHSSModel {

    private SimpleIntegerProperty id = new SimpleIntegerProperty();
    private SimpleBooleanProperty isCheck = new SimpleBooleanProperty();
    private SimpleDoubleProperty freqMin = new SimpleDoubleProperty();
    private SimpleDoubleProperty freqMax = new SimpleDoubleProperty();
    private InterferenceParams interferenceParams = new InterferenceParams();

    public SuppressFHSSModel(){

    }

    public SuppressFHSSModel(int id, boolean isCheck, double freqMin, double freqMax, InterferenceParams interferenceParams) {
        this.id = new SimpleIntegerProperty(id);
        this.isCheck = new SimpleBooleanProperty(isCheck);
        this.freqMin = new SimpleDoubleProperty(freqMin);
        this.freqMax = new SimpleDoubleProperty(freqMax);
        this.interferenceParams = new InterferenceParams(interferenceParams);
    }

    public int getId() { return id.get(); }
    public void setId(int value) { id.set(value); }

    public SimpleBooleanProperty isCheckProperty() { return isCheck; }
    public Boolean getIsCheck(){ return isCheck.get();}
    public void setIsCheck(boolean value){ isCheck.set(value);}

    public double getFreqMin() { return freqMin.get(); }
    public SimpleDoubleProperty freqMinProperty() { return freqMin; }
    public void setFreqMin(double freqMin) { this.freqMin.set(freqMin); }

    public double getFreqMax() { return freqMax.get(); }
    public SimpleDoubleProperty freqMaxProperty() { return freqMax; }
    public void setFreqMax(double freqMax) { this.freqMax.set(freqMax); }

    public InterferenceParams getInterferenceParams() { return interferenceParams; }
    public void setInterferenceParams(InterferenceParams interferenceParams) { this.interferenceParams = interferenceParams; }

}
