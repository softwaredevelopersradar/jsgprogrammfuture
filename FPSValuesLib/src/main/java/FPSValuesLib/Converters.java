package FPSValuesLib;

import FPSModels.Led;
import FPSValuesLib.Translation.CMeaning;
import FPSValuesLib.Translation.CTypeModulation;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Converters {
    public static ImageView LedConverter(Led led) {
        if (led == null) {
            return new ImageView();
        } else {
            ImageView imageView = new ImageView();
            imageView.setFitHeight(15.0D);
            imageView.setFitWidth(15.0D);
            switch(led) {
                case Empty:
                    imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/empty.png")));
                    break;
                case Gray:
                    imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/gray.png")));
                    break;
                case Green:
                    imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/green.png")));
                    break;
                case Red:
                    imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/red.png")));
                    break;
                case Blue:
                    imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/blue.png")));
                    break;
                case Yellow:
                    imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/yellow.png")));
            }

            return imageView;
        }
    }
    public static ImageView LedConverter1(Led led) {
        if (led == null) {
            return new ImageView();
        } else {
            ImageView imageView = new ImageView();
            imageView.setFitHeight(15.0D);
            imageView.setFitWidth(15.0D);
            switch(led) {
                case Green:
                imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/green1.png")));
                break;
            }
            return imageView;
        }
    }
    public static ImageView LedConverter2(Led led) {
        if (led == null) {
            return new ImageView();
        } else {
            ImageView imageView = new ImageView();
            imageView.setFitHeight(15.0D);
            imageView.setFitWidth(15.0D);
            switch(led) {
                case Green:
                    imageView.setImage(new Image(Converters.class.getResourceAsStream("Resources/green2.png")));
                    break;
            }
            return imageView;
        }
    }


    public static String  ErrorsConverter (byte Error)
    {
    switch (Byte.toUnsignedInt(Error))
        {
            case 0: return "Ошибок нет";
            case 1: return "Отказ ИП +24В системы управления";
            case 3: return "Отказ ИП +24В предварительного усилителя";
            case 4: return "Отказ ИП +48В";
            case 5: return "Отказ панели управления";
            case 6: return "Отказ блока фильтров";
            case 7: return "Отказ интерфейса CAN";
            case 8: return "Ошибка обмена/нет ответа с/от БФ";
            case 9: return "Ошибка обмена/нет ответа с/от ИП1";
            case 10: return "Ошибка обмена/нет ответа с/от ИП2";
            case 11: return "Ошибка обмена/нет ответа с/от ИП3";
            case 12: return "Ошибка обмена/нет ответа с/от ИП4";
            case 13: return "Ошибка обмена/нет ответа с/от БСМ1";
            case 14: return "Ошибка обмена/нет ответа с/от БСМ2";
            case 15: return "Ошибка обмена/нет ответа с/от БСМ3";
            case 16: return "Ошибка обмена/нет ответа с/от БСМ4";
            case 17: return "Нет выходного напряжения УМ1";
            case 18: return "Ошибка обмена/нет ответа с/от УМ1";
            case 19: return "Нет питания транзисторов блока УМ1";
            case 20: return "Перегрузка по току УМ1";
            case 21: return "Разбаланс УМ1";
            case 22: return "Перегрев УМ1";
            case 25: return "Нет выходного напряжения УМ2";
            case 26: return "Ошибка обмена/нет ответа с/от УМ2";
            case 27: return "Нет питания транзисторов блока УМ2";
            case 28: return "Перегрузка по току УМ2";
            case 29: return "Разбаланс УМ2";
            case 30: return "Перегрев УМ2";
            case 33: return "Нет выходного напряжения УМ3";
            case 34: return "Ошибка обмена/нет ответа с/от УМ3";
            case 35: return "Нет питания транзисторов блока УМ3";
            case 36: return "Перегрузка по току УМ3";
            case 37: return "Разбаланс УМ3";
            case 38: return "Перегрев УМ3";
            case 41: return "Нет выходного напряжения УМ4";
            case 42: return "Ошибка обмена/нет ответа с/от УМ4";
            case 43: return "Нет питания транзисторов блока УМ4";
            case 44: return "Перегрузка по току УМ4";
            case 45: return "Разбаланс УМ4";
            case 46: return "Перегрев УМ4";
            case 49: return "Нет выходного напряжения УМ5";
            case 50: return "Ошибка обмена/нет ответа с/от УМ5";
            case 51: return "Нет питания транзисторов блока УМ5";
            case 52: return "Перегрузка по току УМ5";
            case 53: return "Разбаланс УМ5";
            case 54: return "Перегрев УМ5";
            case 57: return "Нет выходного напряжения УМ6";
            case 58: return "Ошибка обмена/нет ответа с/от УМ6";
            case 59: return "Нет питания транзисторов блока УМ6";
            case 60: return "Перегрузка по току УМ6";
            case 61: return "Разбаланс УМ6";
            case 62: return "Перегрев УМ6";
            case 65: return "Нет выходного напряжения УМ7";
            case 66: return "Ошибка обмена/нет ответа с/от УМ7";
            case 67: return "Нет питания транзисторов блока УМ7";
            case 68: return "Перегрузка по току УМ7";
            case 69: return "Разбаланс УМ7";
            case 70: return "Перегрев УМ7";
            case 73: return "Нет выходного напряжения УМ8";
            case 74: return "Ошибка обмена/нет ответа с/от УМ8";
            case 75: return "Нет питания транзисторов блока УМ8";
            case 76: return "Перегрузка по току УМ8";
            case 77: return "Разбаланс УМ8";
            case 78: return "Перегрев УМ8";
            case 81: return "Нет выходного напряжения УМ9";
            case 82: return "Ошибка обмена/нет ответа с/от УМ9";
            case 83: return "Нет питания транзисторов блока УМ9";
            case 84: return "Перегрузка по току УМ9";
            case 85: return "Разбаланс УМ9";
            case 86: return "Перегрев УМ9";
            case 89: return "Нет выходного напряжения УМ10";
            case 90: return "Ошибка обмена/нет ответа с/от УМ10";
            case 91: return "Нет питания транзисторов блока УМ10";
            case 92: return "Перегрузка по току УМ10";
            case 93: return "Разбаланс УМ10";
            case 94: return "Перегрев УМ10";
            case 97: return "Нет выходного напряжения УМ11";
            case 98: return "Ошибка обмена/нет ответа с/от УМ11";
            case 99: return "Нет питания транзисторов блока УМ11";
            case 100: return "Перегрузка по току УМ11";
            case 101: return "Разбаланс УМ11";
            case 102: return "Перегрев УМ11";
            case 105: return "Нет выходного напряжения УМ12";
            case 106: return "Ошибка обмена/нет ответа с/от УМ12";
            case 107: return "Нет питания транзисторов блока УМ12";
            case 108: return "Перегрузка по току УМ12";
            case 109: return "Разбаланс УМ12";
            case 110: return "Перегрев УМ12";
            case 113: return "Нет выходного напряжения УМ13";
            case 114: return "Ошибка обмена/нет ответа с/от УМ13";
            case 115: return "Нет питания транзисторов блока УМ13";
            case 116: return "Перегрузка по току УМ13";
            case 117: return "Разбаланс УМ13";
            case 118: return "Перегрев УМ13";
            case 121: return "Нет выходного напряжения УМ14";
            case 122: return "Ошибка обмена/нет ответа с/от УМ14";
            case 123: return "Нет питания транзисторов блока УМ14";
            case 124: return "Перегрузка по току УМ14";
            case 125: return "Разбаланс УМ14";
            case 126: return "Перегрев УМ14";
            case 129: return "Нет выходного напряжения УМ15";
            case 130: return "Ошибка обмена/нет ответа с/от УМ15";
            case 131: return "Нет питания транзисторов блока УМ15";
            case 132: return "Перегрузка по току УМ15";
            case 133: return "Разбаланс УМ15";
            case 134: return "Перегрев УМ15";
            case 137: return "Нет выходного напряжения УМ16";
            case 138: return "Ошибка обмена/нет ответа с/от УМ16";
            case 139: return "Нет питания транзисторов блока УМ16";
            case 140: return "Перегрузка по току УМ16";
            case 141: return "Разбаланс УМ16";
            case 142: return "Перегрев УМ16";

        }

        return "";
    }
    /**
     * декодирование параметров помехи из кодов в строку
     * @param bModulation модуляция
     * @param bDeviation девиация
     * @param bManipulation манипуляция
     * @param bDuration длительность
     * @return
     */
    public static String StrInterferenceParams(byte bModulation, byte bDeviation, byte bManipulation, byte bDuration)
    {
        String strModulation = "";    // модуляция
        String strManipulation = "";  // манипуляция
        String strSpectrumWidth = ""; // ширина спектра
        String strFreqSpacing = "";   // разнос по частоте
        String strDuration = "";      // длительность

        String strInterferenceParams = "";    // все параметры

        switch (bModulation) {

            case 1:
                strModulation = CTypeModulation.paramChMSh; // "ЧМШ";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "0.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "1 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "1.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "2 " + CMeaning.meaningkHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = "3.4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = "4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = "8 " + CMeaning.meaningkHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = "16 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }

                break;

            case 2:
                strModulation = CTypeModulation.paramChMShScan; // "ЧМШ скан. f";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "0.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "1 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "1.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "2 " + CMeaning.meaningkHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = "3.4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = "4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = "8 " + CMeaning.meaningkHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = "16 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 3:
                strModulation = CTypeModulation.paramChM2; // "ЧМн-2";

                if(bDeviation > 0) {

                    strFreqSpacing = bDeviation * 10 + " " + CMeaning.meaningHz + " ";
                }

                switch (bManipulation)
                {
                    case 1:
                        strManipulation = "2000 " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strManipulation = "1000 " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strManipulation = "500 " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strManipulation = "200 " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strManipulation = "100 " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strManipulation = "50 " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strManipulation = "20 " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strManipulation = "10 " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strManipulation = "5 " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 4:
                strModulation = CTypeModulation.paramChM4; // "ЧМн-4";

                if(bDeviation > 0) {

                    strFreqSpacing = bDeviation * 10 + " " + CMeaning.meaningHz + " ";
//                    String hexNum = String.valueOf(bDeviation);
//                    int num = Integer.parseInt(hexNum,16);
//                    strFreqSpacing = num * 10 + " " + CMeaning.meaningHz + " ";
                }

                switch (bManipulation)
                {
                    case 1:
                        strManipulation = "2000 " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strManipulation = "1000 " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strManipulation = "500 " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strManipulation = "200 " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strManipulation = "100 " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strManipulation = "50 " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strManipulation = "20 " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strManipulation = "10 " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strManipulation = "5 " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 5:
                strModulation = CTypeModulation.paramChM8; // "ЧМн-8";

                if(bDeviation > 0) {

                    strFreqSpacing = bDeviation * 10 + " " + CMeaning.meaningHz + " ";
                }

                switch (bManipulation)
                {
                    case 1:
                        strManipulation = "2000 " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strManipulation = "1000 " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strManipulation = "500 " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strManipulation = "200 " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strManipulation = "100 " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strManipulation = "50 " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strManipulation = "20 " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strManipulation = "10 " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strManipulation = "5 " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 6:
                strModulation = CTypeModulation.paramFM2; // "ФМн-2";

                switch (bManipulation)
                {
                    case 1:
                        strManipulation = "100000 " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strManipulation = "50000 " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strManipulation = "20000 " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strManipulation = "10000 " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strManipulation = "5000 " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strManipulation = "2400 " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strManipulation = "2000 " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strManipulation = "1200 " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strManipulation = "1000 " + CMeaning.meaningHz + " ";
                        break;

                    case 0x0A:
                        strManipulation = "500 " + CMeaning.meaningHz + " ";
                        break;

                    case 0x0B:
                        strManipulation = "250 " + CMeaning.meaningHz + " ";
                        break;

                    case 0x0C:
                        strManipulation = "125 " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 7:
                strModulation = CTypeModulation.paramFM4; // "ФМн-4";

                switch (bManipulation)
                {
                    case 1:
                        strManipulation = "100000 " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strManipulation = "50000 " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strManipulation = "20000 " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strManipulation = "10000 " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strManipulation = "5000 " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strManipulation = "2400 " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strManipulation = "2000 " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strManipulation = "1200 " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strManipulation = "1000 " + CMeaning.meaningHz + " ";
                        break;

                    case 0x0A:
                        strManipulation = "500 " + CMeaning.meaningHz + " ";
                        break;

                    case 0x0B:
                        strManipulation = "250 " + CMeaning.meaningHz + " ";
                        break;

                    case 0x0C:
                        strManipulation = "125 " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 8:
                strModulation = CTypeModulation.paramFM8; // "ФМн-8";

                switch (bManipulation)
                {
                    case 1:
                        strManipulation = "100000 " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strManipulation = "50000 " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strManipulation = "20000 " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strManipulation = "10000 " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strManipulation = "5000 " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strManipulation = "2400 " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strManipulation = "2000 " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strManipulation = "1200 " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strManipulation = "1000 " + CMeaning.meaningHz + " ";
                        break;

                    case 0x0A:
                        strManipulation = "500 " + CMeaning.meaningHz + " ";
                        break;

                    case 0x0B:
                        strManipulation = "250 " + CMeaning.meaningHz + " ";
                        break;

                    case 0x0C:
                        strManipulation = "125 " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 9:
                strModulation = CTypeModulation.paramZSh; // "ЗШ";

                switch (bManipulation)
                {
                    case 1:
                        strSpectrumWidth = "2500 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "1000 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "500 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "200 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 0x0C:
                strModulation = CTypeModulation.paramAFMn; // "АФМн";

                switch (bManipulation)
                {
                    case 1:
                        strManipulation = "100000 " + CMeaning.meaningHz + " ";
                        break;

                    case 2:
                        strManipulation = "50000 " + CMeaning.meaningHz + " ";
                        break;

                    case 3:
                        strManipulation = "20000 " + CMeaning.meaningHz + " ";
                        break;

                    case 4:
                        strManipulation = "10000 " + CMeaning.meaningHz + " ";
                        break;

                    case 5:
                        strManipulation = "5000 " + CMeaning.meaningHz + " ";
                        break;

                    case 6:
                        strManipulation = "2000 " + CMeaning.meaningHz + " ";
                        break;

                    case 7:
                        strManipulation = "1000 " + CMeaning.meaningHz + " ";
                        break;

                    case 8:
                        strManipulation = "500 " + CMeaning.meaningHz + " ";
                        break;

                    case 9:
                        strManipulation = "200 " + CMeaning.meaningHz + " ";
                        break;

                    default:
                        break;
                }
                break;

            case 0x0D:
                strModulation = CTypeModulation.paramChM; // "ЧМ";

                switch (bDeviation)
                {
                    case 1:
                        strSpectrumWidth = "0.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 2:
                        strSpectrumWidth = "1 " + CMeaning.meaningkHz + " ";
                        break;

                    case 3:
                        strSpectrumWidth = "1.5 " + CMeaning.meaningkHz + " ";
                        break;

                    case 4:
                        strSpectrumWidth = "2 " + CMeaning.meaningkHz + " ";
                        break;

                    case 5:
                        strSpectrumWidth = "3.4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 6:
                        strSpectrumWidth = "4 " + CMeaning.meaningkHz + " ";
                        break;

                    case 7:
                        strSpectrumWidth = "8 " + CMeaning.meaningkHz + " ";
                        break;

                    case 8:
                        strSpectrumWidth = "16 " + CMeaning.meaningkHz + " ";
                        break;

                    default:
                        break;
                }
                break;
        }

        switch (bDuration) {

            case 1:
                strDuration = "0.5 " + CMeaning.meaningms + " ";
                break;

            case 2:
                strDuration = "1 " + CMeaning.meaningms + " ";
                break;

            case 3:
                strDuration = "1.5 " + CMeaning.meaningms + " ";
                break;

            case 4:
                strDuration = "2 " + CMeaning.meaningms + " ";
                break;

            default:
                strDuration = "";
                break;
        }

        strInterferenceParams = strModulation + " " + strSpectrumWidth + " " + strManipulation + " " + strFreqSpacing + " " + strDuration;

        return strInterferenceParams;
    }
}

