package FPSValuesLib;

public class PropertiesValues {

    public static Byte indexModulation = 1;
    public static Byte indexManipulation = 1;
    public static Byte indexDeviation = 1;
    public static Byte indexDuration = 1;
    public static Integer valueFreqSpacing = 10;
    public static Integer valueDeviation = 0;

    public static Byte getIndexModulation() { return indexModulation; }
    public static void setIndexModulation(Byte indexModulation) { PropertiesValues.indexModulation = indexModulation; }

    public static Byte getIndexManipulation() { return indexManipulation; }
    public static void setIndexManipulation(Byte indexManipulation) { PropertiesValues.indexManipulation = indexManipulation; }

    public static Byte getIndexDeviation() { return indexDeviation; }
    public static void setIndexDeviation(Byte indexDeviation) { PropertiesValues.indexDeviation = indexDeviation; }

    public static Byte getIndexDuration() { return indexDuration; }
    public static void setIndexDuration(Byte indexDuration) { PropertiesValues.indexDuration = indexDuration; }

    public static Integer getValueFreqSpacing() { return valueFreqSpacing; }
    public static void setValueFreqSpacing(Integer valueFreqSpacing) { PropertiesValues.valueFreqSpacing = valueFreqSpacing; }

    public static Integer getValueDeviation() { return valueDeviation; }
    public static void setValueDeviation(Integer valueDeviation) { PropertiesValues.valueDeviation = valueDeviation; }
}
