package FPSValuesLib.Suppress;


import FPSModels.InterferenceParams;
import javafx.scene.control.ComboBox;

public class InitInterferenceParams {

    /**
     * виды модуляции
     * @param cb
     */
    public static void InitModulation(ComboBox cb)
    {
        cb.getItems().clear();

        cb.getItems().add("ЧМШ");
        cb.getItems().add("ЧМШ скан. f");
        cb.getItems().add("ЧМн-2");
        cb.getItems().add("ЧМн-4");
        cb.getItems().add("ЧМн-8");
        cb.getItems().add("ФМн-2");
        cb.getItems().add("ФМн-4");
        cb.getItems().add("ФМн-8");
        cb.getItems().add("ЗШ");
        cb.getItems().add("АФМн");
        cb.getItems().add("ЧМ");

        cb.getSelectionModel().select(0);
    }

    /**
     * значения длительности
     * @param cb
     */
    public static void InitDuration(ComboBox cb)
    {
        cb.getItems().clear();

        cb.getItems().add("0.5");
        cb.getItems().add("1");
        cb.getItems().add("1.5");
        cb.getItems().add("2");

        cb.getSelectionModel().select(0);
    }

    /**
     * возможные виды девиации (ширины спектра), кГц
     * @param cb
     * @param countItems
     */
    public static void InitDeviation(ComboBox cb, int countItems)
    {
        cb.getItems().clear();

        switch (countItems)
        {
            case 4:
                cb.getItems().add("2500");
                cb.getItems().add("1000");
                cb.getItems().add("500");
                cb.getItems().add("200");

                cb.getSelectionModel().select(0);

                break;

            case 8:
                cb.getItems().add("0.5");
                cb.getItems().add("1");
                cb.getItems().add("1.5");
                cb.getItems().add("2");
                cb.getItems().add("3.4");
                cb.getItems().add("4");
                cb.getItems().add("8");
                cb.getItems().add("16");

                cb.getSelectionModel().select(0);

                break;
        }
    }

    /**
     * установить возможные виды девиации (ширрины спектра)
     * @param cb
     * @param typeModulation
     */
    public static void SetDeviation(ComboBox cb, byte typeModulation)
    {
        switch (typeModulation)
        {
            case 0: // ЧМШ
            case 1: // ЧМШ скан f
            case 10: //ЧМ
                InitDeviation(cb, 8);
                break;

            case 8: // ЗШ
                InitDeviation(cb, 4);
                break;
        }
    }

    /**
     * возможные значения манипуляции, Гц
     * typeModulation (1 - ЧМ, 2 - ФМ)
     * @param cb
     * @param typeModulation
     */
    public static void InitManipulation(ComboBox cb, byte typeModulation)
    {
        cb.getItems().clear();

        switch (typeModulation)
        {
            case 2: // ЧМн-2
            case 3: // ЧМн-4
            case 4: // ЧМн-8
                cb.getItems().add("2000");
                cb.getItems().add("1000");
                cb.getItems().add("500");
                cb.getItems().add("200");
                cb.getItems().add("100");
                cb.getItems().add("50");
                cb.getItems().add("20");
                cb.getItems().add("10");
                cb.getItems().add("5");

                cb.getSelectionModel().select(0);

                break;

            case 5: // ФМн-2
            case 6: // ФМн-4
            case 7: // ФМн-8
                cb.getItems().add("100000");
                cb.getItems().add("50000");
                cb.getItems().add("20000");
                cb.getItems().add("10000");
                cb.getItems().add("5000");
                cb.getItems().add("2400");
                cb.getItems().add("2000");
                cb.getItems().add("1200");
                cb.getItems().add("1000");
                cb.getItems().add("500");
                cb.getItems().add("250");
                cb.getItems().add("125");

                cb.getSelectionModel().select(0);

                break;

            case 9: // АФМн
                cb.getItems().add("100000");
                cb.getItems().add("50000");
                cb.getItems().add("20000");
                cb.getItems().add("10000");
                cb.getItems().add("5000");
                cb.getItems().add("2000");
                cb.getItems().add("1000");
                cb.getItems().add("500");
                cb.getItems().add("200");

                cb.getSelectionModel().select(0);

                break;
        }
    }

    /**
     * установить возможные значения манипуляции
     * typeModulation (ЧМ, ФМ, АФМн)
     * @param cb
     * @param typeModulation
     */
    public static void SetManipulation(ComboBox cb, byte typeModulation)
    {
        switch (typeModulation)
        {
            case 2: // ЧМн-2
            case 3: // ЧМн-4
            case 4: // ЧМн-8
            case 5: // ФМн-2
            case 6: // ФМн-4
            case 7: // ФМн-8
            case 9: // АФМн
                InitManipulation(cb, typeModulation);
                break;
        }
    }

    /**
     * Получить параметры помехи
     * @param indexModulation индекс в ComboBox (Вид модуляции)
     * @param indexDeviation индекс в ComboBox (Ширина спектра)
     * @param indexManipulation индекс в ComboBox (Частота манипуляции)
     * @param indexDuration индекс в ComboBox (Длительность излучения)
     * @param byteFreqSpacing разнос по частоте для ЧМ-2, ЧМ-4, ЧМ-8 (записывается в Deviation)
     * @return
     */
    public static InterferenceParams GetInterferenceParams(int indexModulation, int indexDeviation, int indexManipulation, int indexDuration, int byteFreqSpacing)
    {
        double dDev = 0;
        InterferenceParams interferenceParams = new InterferenceParams();

        try {

            switch (indexModulation)
            {
                // ЧМШ
                case 0:
                    interferenceParams.modulation = 0x01;
                    interferenceParams.deviation = Byte.parseByte(String.valueOf(indexDeviation + 1));
                    interferenceParams.manipulation = 0;
                    break;

                // ЧМШ сканирующая по частоте на ширину девиации с периодом 0,5 с
                case 1:
                    interferenceParams.modulation = 0x02;
                    interferenceParams.deviation = Byte.parseByte(String.valueOf(indexDeviation + 1));
                    interferenceParams.manipulation = 0;
                    break;

                // ЧМн-2
                case 2:
                    interferenceParams.modulation = 0x03;

                    // Здесь проверить, соответствие девиации манипуляции
                    // if(manipulation < deviation)
//            {
//                Сообщение: "Для выбранной девиации значение манипуляции должно быть больше!";
//                return false;
//            }

                    interferenceParams.deviation = (byte) byteFreqSpacing; //deviation;
                    interferenceParams.manipulation = Byte.parseByte(String.valueOf(indexManipulation + 1));
                    break;

                // ЧМн-4
                case 3:
                    interferenceParams.modulation = 0x04;

                    interferenceParams.deviation = (byte) byteFreqSpacing; //deviation;

                    // Здесь проверить, соответствие девиации манипуляции
                    // if(manipulation < deviation)
//            {
//                Сообщение: "Для выбранной девиации значение манипуляции должно быть больше!";
//                return false;
//            }

                    interferenceParams.manipulation = Byte.parseByte(String.valueOf(indexManipulation + 1));
                    break;

                // ЧМн-8
                case 4:
                    interferenceParams.modulation = 0x05;
                    interferenceParams.deviation = (byte) byteFreqSpacing; //deviation;

                    // Здесь проверить, соответствие девиации манипуляции
                    // if(manipulation < deviation)
//            {
//                Сообщение: "Для выбранной девиации значение манипуляции должно быть больше!";
//                return false;
//            }

                    interferenceParams.manipulation = Byte.parseByte(String.valueOf(indexManipulation + 1));
                    break;

                // ФМн-2
                case 5:
                    interferenceParams.modulation = 0x06;
                    interferenceParams.deviation = 0;
                    interferenceParams.manipulation = Byte.parseByte(String.valueOf(indexManipulation + 1));
                    break;

                // ФМн-4
                case 6:
                    interferenceParams.modulation = 0x07;
                    interferenceParams.deviation = 0;
                    interferenceParams.manipulation = Byte.parseByte(String.valueOf(indexManipulation + 1));
                    break;

                // ФМн-8
                case 7:
                    interferenceParams.modulation = 0x08;
                    interferenceParams.deviation = 0;
                    interferenceParams.manipulation = Byte.parseByte(String.valueOf(indexManipulation + 1));
                    break;

                // ЗШ (КФМ)
                case 8:
                    interferenceParams.modulation = 0x09;
                    interferenceParams.deviation = 0;
                    interferenceParams.manipulation = Byte.parseByte(String.valueOf(indexDeviation + 1));
//                    interferenceParams.manipulation = Byte.parseByte(String.valueOf(valueDeviation));

                    break;

                // АФМн
                case 9:
                    interferenceParams.modulation = 0x0C;
                    interferenceParams.deviation = 0;
                    interferenceParams.manipulation = Byte.parseByte(String.valueOf(indexManipulation + 1));

                    break;
                //ЧМ
                case 10:
                    interferenceParams.modulation = 0x0D;
                    interferenceParams.deviation = Byte.parseByte(String.valueOf(indexDeviation + 1));
                    interferenceParams.manipulation = 0;

                    break;

                default:
                    break;
            }

            interferenceParams.duration = Byte.parseByte(String.valueOf(indexDuration + 1));
        }
        catch (Exception ex)
        {

        }
        return interferenceParams;
    }
}
