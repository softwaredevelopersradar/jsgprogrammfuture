package FPSValuesLib;

public class Properties {
    private int selectedRow;

    public Properties() {
    }

    public int getSelectedRow() {
        return this.selectedRow;
    }

    public void setSelectedRow(int selectedRow) {
        this.selectedRow = selectedRow;
    }
}
