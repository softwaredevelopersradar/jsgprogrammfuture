package FPSValuesLib;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;

public class Values {
    public static RadialGradient RadialGradientRed;
    public static RadialGradient RadialGradientGreen;
    public static RadialGradient RadialGradientGrey;

    public Values() {
    }
    static {
        RadialGradientRed = new RadialGradient(0.0D, 0.0D, 0.5D, 0.5D, 7.0D, false, CycleMethod.NO_CYCLE, new Stop[]{new Stop(0.0D, Color.rgb(255, 255, 255, 1.0D)), new Stop(1.0D, Color.RED)});
        RadialGradientGreen = new RadialGradient(0.0D, 0.0D, 0.5D, 0.5D, 7.0D, false, CycleMethod.NO_CYCLE, new Stop[]{new Stop(0.0D, Color.rgb(255, 255, 255, 1.0D)), new Stop(1.0D, Color.DARKGREEN)});
        RadialGradientGrey = new RadialGradient(0.0D, 0.0D, 0.5D, 0.5D, 7.0D, false, CycleMethod.NO_CYCLE, new Stop[]{new Stop(0.0D, Color.rgb(255, 255, 255, 1.0D)), new Stop(1.0D, Color.DARKGREY)});
    }

}
