package ValuesCorrectLib.Sectors;

import javafx.scene.control.Alert;

public class CorrectParamsSectors {

    /**
     * Проверка на корректность ввода значений сектора
     * @param dAngleMin начальное значение
     * @param dAngleMax конечное значение
     * @return true - успешно, false - нет
     */
    public static Boolean IsCorrectAngleMinMax(double dAngleMin, double dAngleMax)
    {
        Boolean bCorrect = true;

        if (dAngleMin >= dAngleMax)
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Сообщение!");
            alert.setHeaderText(null);
            alert.setContentText("Значение поля 'θ мин.' должно быть меньше значения поля 'θ макс.'!");
            alert.show();

            bCorrect = false;
        }
        return bCorrect;
    }
}
