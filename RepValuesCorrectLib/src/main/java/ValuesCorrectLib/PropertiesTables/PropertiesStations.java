package ValuesCorrectLib.PropertiesTables;

public class PropertiesStations {

    private static int selectedNumASP = 0;

    public static int getSelectedNumASP() { return selectedNumASP; }
    public static void setSelectedNumASP(int selectedNumASP) {

        if(PropertiesStations.selectedNumASP == selectedNumASP) { return; }
        PropertiesStations.selectedNumASP = selectedNumASP;
    }
}
