package ValuesCorrectLib.Suppress;

import KvetkaModels.AnalogSignalType;
import KvetkaModels.DigitalSignalClass;
import KvetkaModels.InterferenceParams;

public class GetInterferenceParamsForSignal {

    /**
     * Определение параметров помехи для аналогового сигнала
     * @param typeSignal тип сигнала
     * @return Параметры помехи
     */
    public static InterferenceParams ForAnalogSignal(AnalogSignalType typeSignal) {

        InterferenceParams interferenceParams = new InterferenceParams();

        switch (typeSignal) {

            case AM -> {

                interferenceParams.modulation = 1;
                interferenceParams.deviation = 1;
                interferenceParams.manipulation = 0;
                interferenceParams.duration = 1;
            }

            case FM -> {

                interferenceParams.modulation = 1;
                interferenceParams.deviation = 1;
                interferenceParams.manipulation = 0;
                interferenceParams.duration = 2;
            }

            case LSB -> {

                interferenceParams.modulation = 1;
                interferenceParams.deviation = 1;
                interferenceParams.manipulation = 0;
                interferenceParams.duration = 3;
            }

            case USB -> {

                interferenceParams.modulation = 1;
                interferenceParams.deviation = 1;
                interferenceParams.manipulation = 0;
                interferenceParams.duration = 4;
            }
        }

        return interferenceParams;
    }

    /**
     * Определение параметров помехи для цифрового сигнала
     * @param typeSignal тип сигнала
     * @return Параметры помехи
     */
    public static InterferenceParams ForDigitalSignal(DigitalSignalClass typeSignal) {

        InterferenceParams interferenceParams = new InterferenceParams();

        switch (typeSignal) {

            case SC_ST4285 -> {

                interferenceParams.modulation = 6;
                interferenceParams.deviation = 6;
                interferenceParams.manipulation = 4;
                interferenceParams.duration = 4;
            }

            case SC_ST4529 -> {

                interferenceParams.modulation = 6;
                interferenceParams.deviation = 7;
                interferenceParams.manipulation = 5;
                interferenceParams.duration = 4;
            }

            case SC_MIL110A -> {

                interferenceParams.modulation = 6;
                interferenceParams.deviation = 6;
                interferenceParams.manipulation = 4;
                interferenceParams.duration = 4;
            }

            case SC_MIL110B_APPC -> {

                interferenceParams.modulation = 6;
                interferenceParams.deviation = 6;
                interferenceParams.manipulation = 4;
                interferenceParams.duration = 4;
            }

            case SC_CLOVER -> {

                interferenceParams.modulation = 6;
                interferenceParams.deviation = 6;
                interferenceParams.manipulation = 4;
                interferenceParams.duration = 4;
            }

            case SC_CODAN -> {

                interferenceParams.modulation = 6;
                interferenceParams.deviation = 6;
                interferenceParams.manipulation = 4;
                interferenceParams.duration = 4;
            }

            case SC_DC7200 -> {

                interferenceParams.modulation = 6;
                interferenceParams.deviation = 6;
                interferenceParams.manipulation = 4;
                interferenceParams.duration = 4;
            }

            case SC_B162 -> {

                interferenceParams.modulation = 6;
                interferenceParams.deviation = 6;
                interferenceParams.manipulation = 4;
                interferenceParams.duration = 4;
            }


        }


        return interferenceParams;
    }
}
