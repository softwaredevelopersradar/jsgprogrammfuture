package ValuesCorrectLib.Suppress;

import KvetkaModels.SpecFreqsModel;

import java.util.*;
import java.util.stream.Collectors;

public class CorrectParamsSuppressFHSS {

    public static String IsCheckRangeSuppressFHSS(double dFreqMin, double dFreqMax) {

        String sMessage = "";

        if(dFreqMax - dFreqMin > 10000) {

            sMessage = "ИРИ ППРЧ РП не должен превышать 10000 кГц!";
        }
        return sMessage;
    }


    /**
     *  Принадлежность частот заданным диапазонам РП
     * @param dFreqMin частота min
     * @param dFreqMax частота max
     * @param rangesSuppress диапазоны РП
     * @return сообщение об ошибке (пустая строка - успешно)
     */
    public static String IsCheckRangesSuppress(double dFreqMin, double dFreqMax, List<SpecFreqsModel> rangesSuppress)
    {
        List<SpecFreqsModel> newRangesSuppress = NewRangesSuppress(rangesSuppress);

        String sMessage = "";

        long len = newRangesSuppress.stream().count();

        if (len == 0) return sMessage;

        for (int i = 0; i < len; i++) {

            if ((dFreqMin > newRangesSuppress.get(i).getFreqMin() && dFreqMin < newRangesSuppress.get(i).getFreqMax()) ||
                (dFreqMax > newRangesSuppress.get(i).getFreqMin() && dFreqMax < newRangesSuppress.get(i).getFreqMax())) {

                sMessage = "Диапазон не принадлежит диапазону РП!";
                return sMessage;
            }
        }
        return sMessage;
    }

    /**
     * Новый список РП
     * @param rangesSuppress диапазоны РП
     * @return новые диапазоны РП
     */
    private static List<SpecFreqsModel> NewRangesSuppress(List<SpecFreqsModel> rangesSuppress)
    {
        List<SpecFreqsModel> SortFreqMinKHz = rangesSuppress
                .stream().sorted(Comparator.comparing(SpecFreqsModel::getFreqMin))
                .collect(Collectors.toList());

        List<SpecFreqsModel> newRangesSuppress = new ArrayList<>();

        long len = SortFreqMinKHz.stream().count();

        for (int i = 0; i < len; i++) {

            if (i + 1 == len) { return newRangesSuppress; }

            SpecFreqsModel newTable = new SpecFreqsModel();

            if (SortFreqMinKHz.get(i).getFreqMax() != SortFreqMinKHz.get(i + 1).getFreqMin()) {

                newTable.setFreqMin(SortFreqMinKHz.get(i).getFreqMax());
                newTable.setFreqMax(SortFreqMinKHz.get(i + 1).getFreqMin());

                newRangesSuppress.add(newTable);
            }
        }
        return newRangesSuppress;
    }

    /**
     * Принадлежность диапазона Запрещенным частотам
     * @param dFreqMin частота min
     * @param dFreqMax частота max
     * @param freqsForbidden запрещенные частоты
     * @return сообщение об ошибке (пустая строка - успешно)
     */
    public static String IsCheckForbiddenRange(double dFreqMin, double dFreqMax, List<SpecFreqsModel> freqsForbidden)
    {
        String sMessage = "";

        long len = freqsForbidden.stream().count();

        for (int i = 0; i < len; i++) {

            if ((dFreqMin >= freqsForbidden.get(i).getFreqMin() && dFreqMin <= freqsForbidden.get(i).getFreqMax()) ||
                (dFreqMax >= freqsForbidden.get(i).getFreqMin() && dFreqMax <= freqsForbidden.get(i).getFreqMax()) ||
                (dFreqMin <= freqsForbidden.get(i).getFreqMin() && dFreqMax >= freqsForbidden.get(i).getFreqMax())) {

                sMessage = "Диапазон не может быть добавлен, так как запрещенный!";
            }
        }
        return sMessage;
    }
}
