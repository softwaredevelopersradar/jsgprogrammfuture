package ValuesCorrectLib;

import KvetkaModels.Coord;
import javafx.scene.control.Alert;

public class CorrectParams {

    /**
     * Проверка на правильность ввода значений координат:
     * Координаты (широта от −90° до +90°, долгота от −180° до +180°)
     * @param latitude широта
     * @param longitude долгота
     * @return true - успешно, false - нет
     */
    public static Boolean IsCorrectCoords(Double latitude, Double longitude)
    {
        {
            if (latitude < -90 || latitude > 90) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Сообщение!");
                alert.setHeaderText(null);
                alert.setContentText("Неверно задана широта!");
                alert.show();

                return false;
            }

            if (longitude < -180 || longitude > 180) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Сообщение!");
                alert.setHeaderText(null);
                alert.setContentText("Неверно задана долгота!");
                alert.show();

                return false;
            }

            return true;
        }
    }

    /**
     * Проверка на правильность ввода значений координат:
     * Координаты (широта от −90° до +90°, долгота от −180° до +180°)
     * @param coord координаты
     * @return true - успешно, false - нет
     */
    public static Boolean IsCorrectCoords(Coord coord)
    {
        {
            if (coord.latitude < -90 || coord.latitude > 90) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Сообщение!");
                alert.setHeaderText(null);
                alert.setContentText("Неверно задана широта!");
                alert.show();

                return false;
            }

            if (coord.longitude < -180 || coord.longitude > 180) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Сообщение!");
                alert.setHeaderText(null);
                alert.setContentText("Неверно задана долгота!");
                alert.show();

                return false;
            }

            return true;
        }
    }

    /**
     * Проверка на правильность ввода значения порога:
     * Порог (от −130 до 0)
     * @param threshold  порог
     * @return true - успешно, false - нет
     */
    public static Boolean IsCorrectThreshold(Integer threshold)
    {
        {
            if (threshold < -130 || threshold > 0) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Сообщение!");
                alert.setHeaderText(null);
                alert.setContentText("Неверно задан порог!");
                alert.show();

                return false;
            }

            return true;
        }
    }
}
