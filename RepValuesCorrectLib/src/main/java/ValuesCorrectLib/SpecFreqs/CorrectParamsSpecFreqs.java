package ValuesCorrectLib.SpecFreqs;

import KvetkaModels.SpecFreqsModel;
import javafx.scene.control.Alert;

import java.util.ArrayList;
import java.util.List;

public class CorrectParamsSpecFreqs {

    /**
     * Проверка на правильность ввода значений:
     * если Fmin меньше 1500 или больше 30000, Fmin = 1500;
     * если Fmax меньше 1500 или больше 30000, Fmax = 30000;
     * @param specFreqsModel Fmin, Fmax
     */
    public static void IsCorrectMinMax(SpecFreqsModel specFreqsModel)
    {
        if (specFreqsModel.getFreqMin() < 1500.0 || specFreqsModel.getFreqMin() > 30000.0) { specFreqsModel.setFreqMin(1500.0); }
        if (specFreqsModel.getFreqMax() < 1500.0 || specFreqsModel.getFreqMax() > 30000.0) { specFreqsModel.setFreqMax(30000.0); }
    }

    public static void IsCorrectMinMax(Double Fmin, Double Fmax)
    {
        if (Fmin < 1500.0 || Fmin > 30000.0) { Fmin = 25.0; }
        if (Fmax < 1500.0 || Fmax > 30000.0) { Fmax = 30000.0; }
    }

    /**
     * Проверка на корректность ввода значений частоты
     * @param dFreqMin начальное значение частоты
     * @param dFreqMax конечное значение частоты
     * @return true - успешно, false - нет
     */
    public static Boolean IsCorrectFreqMinMax(double dFreqMin, double dFreqMax)
    {
        Boolean bCorrect = true;

        if (dFreqMin >= dFreqMax)
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Сообщение!");
            alert.setHeaderText(null);
            alert.setContentText("Значение поля 'F мин.' должно быть меньше значения поля 'F макс.'!");
            alert.show();

            bCorrect = false;
        }

        return bCorrect;
    }

    public static Boolean IsAddRecordsToList(List<SpecFreqsModel> collectionTemp, List<SpecFreqsModel> collectionFrom)
    {
        try
        {
            List<SpecFreqsModel> listTemp = new ArrayList<>(collectionTemp);
            List<SpecFreqsModel> listFrom = new ArrayList<>(collectionFrom);

            for (int i = 0; i < listFrom.stream().count(); i++) {

                if (IsCorrectFreqMinMax(listFrom.get(i).getFreqMin(), listFrom.get(i).getFreqMax())) {

                    if (IsCorrectJoinRanges(listTemp, listFrom.get(i))) {

                        return true;
                    }
                }
            }
        }
        catch (Exception ex) { return false; }

        return false;
    }

    /**
     * Проверка на возможность объединения диапазонов при добавлении или изменении записи
     * @param listSpecFreqs текущие значения
     * @param specFreqs значения, которые надо проверить
     * @return true - успешно, false - нет
     */
    private static Boolean IsCorrectJoinRanges(List<SpecFreqsModel> listSpecFreqs, SpecFreqsModel specFreqs)
    {
        for (int i = 0; i < listSpecFreqs.stream().count(); i++)
        {
            int cmp1 = ((Double)listSpecFreqs.get(i).getFreqMin()).compareTo(specFreqs.getFreqMin());
            int cmp2 = ((Double)listSpecFreqs.get(i).getFreqMax()).compareTo(specFreqs.getFreqMin());
            int cmp3 = ((Double)listSpecFreqs.get(i).getFreqMin()).compareTo(specFreqs.getFreqMax());
            int cmp4 = ((Double)listSpecFreqs.get(i).getFreqMax()).compareTo(specFreqs.getFreqMax());

            /**
             * с = a.CompareTo(b), если результат сравнения
             * < 0 --> a < b,
             * = 0 --> a = b,
             * > 0 --> a > b.
             */
            if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0))) {

                if ((cmp1 == 0) && (cmp4 == 0)) {

                    return true;
                } else {

                    MessageShow(listSpecFreqs.get(i).getFreqMin(), listSpecFreqs.get(i).getFreqMax());
                    return false;
                }
            } else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0))) {

                MessageShow(listSpecFreqs.get(i).getFreqMin(), listSpecFreqs.get(i).getFreqMax());
                return false;
            } else if (((cmp1 < 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 > 0) || (cmp4 == 0))) {

                MessageShow(listSpecFreqs.get(i).getFreqMin(), listSpecFreqs.get(i).getFreqMax());
                return false;
            } else if (((cmp1 > 0) || (cmp1 == 0)) && (cmp3 < 0) && (cmp2 > 0) && ((cmp4 < 0) || (cmp4 == 0))) {

                MessageShow(listSpecFreqs.get(i).getFreqMin(), listSpecFreqs.get(i).getFreqMax());
                return false;
            }
        }
        return true;
    }

    private static void MessageShow(double freqMin, double freqMax) {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Сообщение!");
        alert.setHeaderText(null);
        alert.setContentText("Диапазон " + freqMin + " - " + freqMax +
                " пересекается с заданным диапазоном! Объедините диапазоны или разбейте их на несколько!");
        alert.show();
    }
}
