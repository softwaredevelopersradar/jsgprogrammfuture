package ValuesCorrectLib;

public class GetParams {

    /**
     * Получить литеру
     * @param dFreq частота
     * @return
     */
    public static Byte GetLetter(double dFreq)
    {
        byte bLetter = 0;

        if (dFreq >= Constants.FREQ_START_LETTER_1 & dFreq < Constants.FREQ_START_LETTER_2)
            bLetter = 1;
        if (dFreq >= Constants.FREQ_START_LETTER_2 & dFreq <= Constants.FREQ_STOP_LETTER_2)
            bLetter = 2;

        return bLetter;
    }

    /**
     * Определить литеры
     * @param dFreqMin чатота мин.
     * @param dFreqMax частота макс.
     * @return
     */
    public static Byte[] GetLetters(double dFreqMin, double dFreqMax)
    {
        Byte[] bMasLetters = null;

        // определить литеру для Fmin
        Byte bLetterMin = GetLetter(dFreqMin);

        // определить литеру для Fmax
        Byte bLetterMax = GetLetter(dFreqMax);

        if (bLetterMin != bLetterMax)
        {
            bMasLetters = new Byte[2];
            bMasLetters[0] = bLetterMin;
            bMasLetters[1] = bLetterMax;
        }
        else if (bLetterMin == bLetterMax)
        {
            bMasLetters = new Byte[1];
            bMasLetters[0] = bLetterMin;
        }

        return bMasLetters;
    }

    /**
     * Преобразование массива литер в строку
     * @param Letters массив литер
     * @return
     */
    public static String SetLetters(Byte[] Letters)
    {
        String sLetters = "";

        if (Letters.length == 1) {

            sLetters = Letters[0].toString();
        }
        else if (Letters.length == 2) {

            if(Letters[0] == Letters[1]) {

                sLetters = Letters[0].toString();
            } else {

                sLetters = Letters[0] + " - " + Letters[1];
            }
        }

        return sLetters;
    }

    /**
     * Рассчет ширины спектра из выделенной строки
     * @param bModulation модуляция
     * @param bDeviation девиация
     * @param bManipulation манипуляция
     * @return значение ширины спектра
     */
    public static Double GetSpectrumWidth(byte bModulation, byte bDeviation, byte bManipulation)
    {
        Double dSpectrumWidth = 0.0;

        switch (bModulation) {

            case 3: // "ЧМ-2";

                switch (bManipulation) {
                    case 1:
                        dSpectrumWidth = (double)(bDeviation * 20 + 5);
                        break;

                    case 2:
                        dSpectrumWidth = (double)(bDeviation * 20 + 10);
                        break;

                    case 3:
                        dSpectrumWidth = (double)(bDeviation * 20 + 20);
                        break;

                    case 4:
                        dSpectrumWidth = (double)(bDeviation * 20 + 50);
                        break;

                    case 5:
                        dSpectrumWidth = (double)(bDeviation * 20 + 100);
                        break;

                    case 6:
                        dSpectrumWidth = (double)(bDeviation * 20 + 200);
                        break;

                    case 7:
                        dSpectrumWidth = (double)(bDeviation * 20 + 500);
                        break;

                    case 8:
                        dSpectrumWidth = (double)(bDeviation * 20 + 1000);
                        break;

                    case 9:
                        dSpectrumWidth = (double)(bDeviation * 20 + 2000);
                        break;

                    default:
                        break;
                }
                break;

            case 4: // "ЧМ-4";

                switch (bManipulation) {
                    case 1:
                        dSpectrumWidth = (double)(3 * bDeviation * 20 + 5);
                        break;

                    case 2:
                        dSpectrumWidth = (double)(3 * bDeviation * 20 + 10);
                        break;

                    case 3:
                        dSpectrumWidth = (double)(3 * bDeviation * 20 + 20);
                        break;

                    case 4:
                        dSpectrumWidth = (double)(3 * bDeviation * 20 + 50);
                        break;

                    case 5:
                        dSpectrumWidth = (double)(3 * bDeviation * 20 + 100);
                        break;

                    case 6:
                        dSpectrumWidth = (double)(3 * bDeviation * 20 + 200);
                        break;

                    case 7:
                        dSpectrumWidth = (double)(3 * bDeviation * 20 + 500);
                        break;

                    case 8:
                        dSpectrumWidth = (double)(3 * bDeviation * 20 + 1000);
                        break;

                    case 9:
                        dSpectrumWidth = (double)(3 * bDeviation * 20 + 2000);
                        break;

                    default:
                        break;
                }
                break;

            case 5: // "ЧМ-8";

                switch (bManipulation) {
                    case 1:
                        dSpectrumWidth = (double)(7 * bDeviation * 20 + 5);
                        break;

                    case 2:
                        dSpectrumWidth = (double)(7 * bDeviation * 20 + 10);
                        break;

                    case 3:
                        dSpectrumWidth = (double)(7 * bDeviation * 20 + 20);
                        break;

                    case 4:
                        dSpectrumWidth = (double)(7 * bDeviation * 20 + 50);
                        break;

                    case 5:
                        dSpectrumWidth = (double)(7 * bDeviation * 20 + 100);
                        break;

                    case 6:
                        dSpectrumWidth = (double)(7 * bDeviation * 20 + 200);
                        break;

                    case 7:
                        dSpectrumWidth = (double)(7 * bDeviation * 20 + 500);
                        break;

                    case 8:
                        dSpectrumWidth = (double)(7 * bDeviation * 20 + 1000);
                        break;

                    case 9:
                        dSpectrumWidth = (double)(7 * bDeviation * 20 + 2000);
                        break;

                    default:
                        break;
                }
                break;
        }

        return dSpectrumWidth / 1000;
    }
}
