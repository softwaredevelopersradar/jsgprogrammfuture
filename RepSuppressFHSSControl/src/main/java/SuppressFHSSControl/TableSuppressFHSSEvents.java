package SuppressFHSSControl;

import FPSModels.SuppressFHSSModel;

public class TableSuppressFHSSEvents {

    private SuppressFHSSModel record;
    private Integer fft;
    private Integer id;
    private Integer Duration;

    public TableSuppressFHSSEvents(SuppressFHSSModel record, int fft, int Duration)
    {
        this.record = record;
        this.fft = fft;
        this.id = record.getId();
        this.Duration = Duration;
    }

    public SuppressFHSSModel getRecord() { return record;}

    public Integer getId() { return id; }

    public Integer getFft() { return fft; }

    public void setFft(int fft) {
        this.fft = fft;
    }

    public Integer getDuration() { return Duration; }
}
