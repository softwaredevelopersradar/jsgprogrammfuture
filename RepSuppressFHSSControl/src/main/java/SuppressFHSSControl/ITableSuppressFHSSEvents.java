package SuppressFHSSControl;

public interface ITableSuppressFHSSEvents {
    public void OnAddRecord(TableSuppressFHSSEvents tableModel);
    public void OnChangeRecord(TableSuppressFHSSEvents tableModel);
    public void OnDeleteRecord(TableSuppressFHSSEvents tableModel);
    public void OnClearRecords(String nameTable);
    public void OnAddFHSSToJSG(TableSuppressFHSSEvents tableModel);
    public void SetRadiationJSGOff(TableSuppressFHSSEvents tablemodel);
}
