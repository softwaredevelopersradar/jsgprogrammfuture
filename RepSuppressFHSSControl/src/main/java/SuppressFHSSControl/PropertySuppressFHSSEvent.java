package SuppressFHSSControl;

import FPSModels.SuppressFHSSModel;

public class PropertySuppressFHSSEvent {

    private SuppressFHSSModel record;
    private Integer id;

    public PropertySuppressFHSSEvent(SuppressFHSSModel record)
    {
        this.record = record;
        this.id = record.getId();
    }

    public SuppressFHSSModel getRecord() { return record; }

    public Integer getId() { return id; }
}
