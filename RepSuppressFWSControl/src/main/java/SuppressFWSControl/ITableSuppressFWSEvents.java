package SuppressFWSControl;

public interface ITableSuppressFWSEvents {

    public void OnAddRecord(TableSuppressFWSEvents tableModel);
    public void OnChangeRecord(TableSuppressFWSEvents tableModel);
    public void OnDeleteRecord(TableSuppressFWSEvents tableModel);
    public void OnClearRecords(String nameTable);
    public void OnAddFWSToJSG(TableSuppressFWSEvents tableModel);
    public void SetRadiationJSGOff(TableSuppressFWSEvents tablemodel);
}
