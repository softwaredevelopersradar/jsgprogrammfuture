package SuppressFWSControl;

import FPSModels.SuppressFWSModel;

public class TableSuppressFWSEvents {

    private SuppressFWSModel record;
    private Integer id;
    private Integer Duration;

    public TableSuppressFWSEvents(SuppressFWSModel record, int Duration)
    {
        this.record = record;
        this.id = record.getId();
        this.Duration = Duration;
    }

    public SuppressFWSModel getRecord() { return record; }

    public Integer getId() { return id; }

    public Integer getDuration() { return Duration; }
}
