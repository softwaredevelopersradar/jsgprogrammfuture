package SuppressFWSControl;

import FPSModels.InterferenceParams;
import FPSModels.SuppressFHSSModel;
import FPSModels.SuppressFWSModel;
import FPSValuesLib.Converters;
import FPSValuesLib.GetParams;
import FPSValuesLib.PropertiesValues;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;


public class SuppressFWSControl extends AnchorPane implements IPropertySuppressFWSEvents, Initializable {

    @FXML public TableColumn<SuppressFWSModel, Integer> idColumn;
    @FXML public TableColumn<SuppressFWSModel, Boolean> isCheckColumn;
    @FXML public TableColumn<SuppressFWSModel, Double> frequencyColumn;
    @FXML public TableColumn<SuppressFWSModel, InterferenceParams> interferenceParamsColumn;

    @FXML public TableView<SuppressFWSModel> tableSuppressFWS;
//    @FXML public FPSControlDebug ucFPSControlDebug;
    @FXML public Spinner<Integer> duration;

//    public GrpcClientModel clientJSG;
    Properties properties = new Properties();
    WndPropertySuppressFWS wndProperty;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss:S");

    private List<ITableSuppressFWSEvents> listeners = new ArrayList();

    public void addListener(ITableSuppressFWSEvents toAdd) {
        this.listeners.add(toAdd);
    }

    public SuppressFWSControl() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SuppressFWS.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {

            fxmlLoader.load();

            Callback<TableColumn<SuppressFWSModel, Boolean>, TableCell<SuppressFWSModel, Boolean>> cellFactory = (
                    TableColumn<SuppressFWSModel, Boolean> p) -> new EditingCell();

        }
        catch (IOException exception) {

            throw new RuntimeException(exception);
        }
    }

    @FXML
    private void AddRecord_Click(ActionEvent event) {

            wndProperty = new WndPropertySuppressFWS();
            wndProperty.modulationField.getSelectionModel().select(5);
            wndProperty.manipulationField.getSelectionModel().select(0);
            wndProperty.deviationField.getSelectionModel().select(0);
            wndProperty.durationField.getSelectionModel().select(0);
            PropertiesValues.setIndexModulation(Byte.parseByte(String.valueOf(wndProperty.modulationField.getSelectionModel().getSelectedIndex())));

            wndProperty.IsAddRec = true;
            wndProperty.addListenerProperty(this);
    }


    //Кнопка отправки параметров ФРЧ и вкл. излучения

    public void AddFWSToJSG_Click(ActionEvent actionEvent) {

        int Duration = duration.getValue();

        SuppressFWSModel model = tableSuppressFWS.getItems().get(0);

        for (ITableSuppressFWSEvents tableEvent : listeners) {
            tableEvent.OnAddFWSToJSG(new TableSuppressFWSEvents(model, Duration));
        }

    }

    public void ClickRadiationOff (ActionEvent event) {
        SuppressFWSModel model = new SuppressFWSModel();
        for (ITableSuppressFWSEvents tableEvent : listeners) {
            tableEvent.SetRadiationJSGOff(new TableSuppressFWSEvents(model, 0));
        }
    }



    @FXML
    private void ChangeRecord_Click(ActionEvent event) {

        if(tableSuppressFWS.getSelectionModel().isEmpty()) {
            return;
        }

        SuppressFWSModel tableModel = tableSuppressFWS.getSelectionModel().getSelectedItem();

        if(tableModel != null) {

            wndProperty = new WndPropertySuppressFWS(tableModel);
            wndProperty.setIdField(tableModel.getId());
            wndProperty.isCheckField = tableModel.getIsCheck();
            wndProperty.freqField.setText(String.valueOf(new DecimalFormat("#0.000").format(tableModel.getFrequency())));
            if(PropertiesValues.getIndexModulation() == 0) { wndProperty.SetDisableFields(PropertiesValues.getIndexModulation()); }
            wndProperty.modulationField.getSelectionModel().select(PropertiesValues.getIndexModulation());
            wndProperty.manipulationField.getSelectionModel().select(PropertiesValues.getIndexManipulation());
            if(PropertiesValues.getIndexModulation() > 1 && PropertiesValues.getIndexModulation() < 5) {
                wndProperty.freqSpacingField.getValueFactory().setValue(PropertiesValues.getValueFreqSpacing());
//                wndProperty.deviationField.getItems().clear();
//                wndProperty.deviationField.getItems().add(String.valueOf(PropertiesValues.getValueDeviation()));
//                wndProperty.deviationField.getSelectionModel().select(0);
            }
            else {
                wndProperty.deviationField.getSelectionModel().select(PropertiesValues.getIndexDeviation());
            }
//            wndProperty.freqSpacingField.getValueFactory().setValue(PropertiesValues.getValueFreqSpacing());
            wndProperty.durationField.getSelectionModel().select(PropertiesValues.getIndexDuration());

            wndProperty.IsChangeRec = true;
            wndProperty.addListenerProperty(this);
        }
    }

    @FXML
    private void DeleteRecord_Click(ActionEvent event) {

        if(tableSuppressFWS.getSelectionModel().isEmpty()) {
            return;
        }

        SuppressFWSModel tableModel = tableSuppressFWS.getSelectionModel().getSelectedItem();

        SuppressFWSModel table = new SuppressFWSModel();
        table.setId(tableModel.getId());
        table.setIsCheck(tableModel.getIsCheck());
        table.setFrequency(tableModel.getFrequency());
        table.setInterferenceParams(tableModel.getInterferenceParams());

    }

    @FXML
    private void ClearRecords_Click(ActionEvent event) {
    ObservableList<SuppressFWSModel> templist = FXCollections.observableArrayList(new SuppressFWSModel[]{});
    tableSuppressFWS.setItems(templist);
    }

    /**
     * Обновить таблицу
     * @param list
     */
    public void UpdateSuppressFWS(List<SuppressFWSModel> list)
    {
        try
        {
            if (list == null)
                return;


            ObservableList<SuppressFWSModel> tempList = FXCollections.observableArrayList(list);

            tableSuppressFWS.setItems(tempList);

            SelectedRow(properties.getSelectedRow());

        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void SelectedRow(int ind)
    {
        tableSuppressFWS.getSelectionModel().clearSelection();
        tableSuppressFWS.requestFocus();
        tableSuppressFWS.getFocusModel().focus(ind);
        tableSuppressFWS.getSelectionModel().select(ind);

        GetSelectedRowParams();
    }

    @FXML
    private void Mouse_Click(MouseEvent event) {

        properties.setSelectedRow(tableSuppressFWS.getSelectionModel().getSelectedIndex());

        GetSelectedRowParams();
    }

    private void GetSelectedRowParams() {

        try {

            if(tableSuppressFWS.getSelectionModel().getSelectedItem() == null) {
                return;
            }

            //modulation АФМн  0x0C hex = 12 dec
            int mod = tableSuppressFWS.getSelectionModel().getSelectedItem().getInterferenceParams().modulation - 1;
            if(mod == 11) mod = 9;
            //modulation ЧМ  0x0D hex = 13 dec
            if(mod == 12) mod = 10;
            PropertiesValues.setIndexModulation((byte)mod);

            int iDev = -1;
            int dev = -1;

            if(mod > 1 && mod < 5) {

               iDev = tableSuppressFWS.getSelectionModel().getSelectedItem().getInterferenceParams().deviation * 10;
                PropertiesValues.setValueFreqSpacing(iDev);
            }
            else {

                dev = tableSuppressFWS.getSelectionModel().getSelectedItem().getInterferenceParams().deviation - 1;
                PropertiesValues.setIndexDeviation((byte)dev);
            }

            int man = tableSuppressFWS.getSelectionModel().getSelectedItem().getInterferenceParams().manipulation - 1;
            PropertiesValues.setIndexManipulation((byte)man);

            int dur = tableSuppressFWS.getSelectionModel().getSelectedItem().getInterferenceParams().duration - 1;
            PropertiesValues.setIndexDuration((byte)dur);

        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Test Add records to list
     */
    public ObservableList<SuppressFWSModel> AddRecToList() {

        // создаем список объектов
        ObservableList<SuppressFWSModel> list = FXCollections.observableArrayList(

        );

        return list;
    }


    public void initialize(URL url, ResourceBundle resourceBundle) {

        idColumn.setCellValueFactory(new PropertyValueFactory<SuppressFWSModel, Integer>("id"));
        isCheckColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SuppressFWSModel, Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<SuppressFWSModel, Boolean> param) {

                SuppressFWSModel model = param.getValue();

                SimpleBooleanProperty booleanProp = new SimpleBooleanProperty(model.getIsCheck());

                booleanProp.addListener(new ChangeListener<Boolean>() {

                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
                                        Boolean newValue) {

                        model.setIsCheck(newValue);

                    }
                });
                return booleanProp;
            }
        });
        frequencyColumn.setCellValueFactory(new PropertyValueFactory<SuppressFWSModel, Double>("frequency"));
        interferenceParamsColumn.setCellValueFactory(new PropertyValueFactory<SuppressFWSModel, InterferenceParams>("interferenceParams"));
        tableSuppressFWS.setPlaceholder(new Label("Записи в таблице отсутствуют!"));


        /**
         * Редактирование
         */
        isCheckColumn.setCellFactory(new Callback<TableColumn<SuppressFWSModel, Boolean>, //
                TableCell<SuppressFWSModel, Boolean>>() {
            @Override
            public TableCell<SuppressFWSModel, Boolean> call(TableColumn<SuppressFWSModel, Boolean> p) {
                CheckBoxTableCell<SuppressFWSModel, Boolean> cell = new CheckBoxTableCell<SuppressFWSModel, Boolean>();
                return cell;
            }
        });

//        frequencyColumn.setCellFactory(new Callback<TableColumn<SuppressFWSModel, Integer>, TableCell<SuppressFWSModel, Integer>>() {
//
//            @Override
//            public TableCell<SuppressFWSModel, Integer> call(TableColumn<SuppressFWSModel, Integer> param) {
//                return new TableCell<SuppressFWSModel, Integer>(){
//
//                    private final Label label;
//                    {
//                        label = new Label();
//                        label.setStyle("-fx-text-fill: white;");
//                    }
//                    @Override
//                    protected void updateItem(int value, boolean empty){
//                        super.updateItem(value, empty);
//                        if (empty){
//                            setGraphic(null);
//                        } else {
//                            label.setText(new DecimalFormat("").format(value));
//                            setGraphic(label);
//                        }
//                    }
//                };
//            }
//        });


        interferenceParamsColumn.setCellFactory(new Callback<TableColumn<SuppressFWSModel, InterferenceParams>, TableCell<SuppressFWSModel, InterferenceParams>>() {

            @Override
            public TableCell<SuppressFWSModel, InterferenceParams> call(TableColumn<SuppressFWSModel, InterferenceParams> param) {
                return new TableCell<SuppressFWSModel, InterferenceParams>(){

                    private final Label label;
                    {
                        label = new Label();
                        label.setStyle("-fx-text-fill: white;");
                    }
                    @Override
                    protected void updateItem(InterferenceParams value, boolean empty){
                        super.updateItem(value, empty);
                        if (empty){
                            setGraphic(null);
                        } else {
                            String strInterferenceParams = Converters.StrInterferenceParams(value.modulation, value.deviation, value.manipulation, value.duration);
                            label.setText(strInterferenceParams);
                            setGraphic(label);
                        }
                    }
                };
            }
        });

   }

    @Override
    public void OnIsAddRecord(Boolean b) {

        if(b) {

            for (ITableSuppressFWSEvents tableEvent : listeners) {

                tableEvent.OnAddRecord(new TableSuppressFWSEvents(wndProperty.suppressFWSModel, 0));
            }
        }

    }

    @Override
    public void OnIsChangeRecord(Boolean b) {

        if(b) {

            for (ITableSuppressFWSEvents tableEvent : listeners) {

                tableEvent.OnChangeRecord(new TableSuppressFWSEvents(wndProperty.suppressFWSModel, 0));
            }
        }
    }
}

class EditingCell extends TableCell<SuppressFWSModel, Boolean> {

    private Boolean textField;

    public EditingCell() {
    }

    @Override
    public void startEdit() {
        super.startEdit();

        if (textField == null) {
            createTextField();
        }


    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText(String.valueOf(getItem()));
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    @Override
    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
//                    textField.setText(getString());
                }
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            } else {
                setContentDisplay(ContentDisplay.TEXT_ONLY);
            }
        }
    }

    private void createTextField() {
//        textField = new Boolean() TextField(getBoolean());
//        textField.setOnKeyPressed(t -> {
//            if (t.getCode() == KeyCode.ENTER) {
//                commitEdit(textField.getText());
//            } else if (t.getCode() == KeyCode.ESCAPE) {
//                cancelEdit();
//            }
//        });
    }

    private Boolean getBoolean() {
        return getItem() == true ? false : getItem();
    }


}