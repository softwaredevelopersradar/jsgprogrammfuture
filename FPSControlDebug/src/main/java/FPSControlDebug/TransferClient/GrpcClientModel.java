package FPSControlDebug.TransferClient;

import PackageJSG.TransmissionGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class GrpcClientModel {
    public TransmissionGrpc.TransmissionBlockingStub blockingStub;
    public TransmissionGrpc.TransmissionStub asyncStub;
    public ManagedChannel channel;

    public GrpcClientModel(String serverIp, int serverPort)
    {
        channel = ManagedChannelBuilder.forAddress(serverIp, serverPort).usePlaintext().build();
        blockingStub = TransmissionGrpc.newBlockingStub(channel);
        asyncStub = TransmissionGrpc.newStub(channel);
    }

}