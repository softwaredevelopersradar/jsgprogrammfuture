package FPSControlDebug.TransferClient;

import PackageJSG.JSG;

public class ClientJSG {
    private String serverIp = "127.0.0.1";
    private int serverPort = 5900;


    private GrpcClientModel clientGrpc;


    public ClientJSG(String serverIp, int serverPort) {
        try {
            this.serverIp = serverIp;
            this.serverPort = serverPort;
        } catch (Exception ignored) {
        }
    }

    public JSG.StateJsgResponseMessage StatusRequest(){
        var answer = clientGrpc.blockingStub.statusRequest(JSG.DefaultRequest.getDefaultInstance());

        return answer;
    }
}
