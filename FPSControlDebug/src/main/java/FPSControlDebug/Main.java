package FPSControlDebug;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {



        static Stage Window;
        static Scene FrontEnd;

        @Override
        public void start(Stage primaryStage) throws Exception{


            Window = primaryStage;
            Initialize();

            Window.setTitle("Состояние ФПС и УМ");
            Window.setResizable(true);
            Window.setScene(FrontEnd);
            Window.setMaxWidth(620);
            Window.setMaxHeight(870);
            Window.show();

        }

        private void Initialize() throws IOException {

            FrontEnd = new Scene(FXMLLoader.load(getClass().getResource("FPS.fxml")));

        }


        public static void main(String[] args) {
            launch(args);
        }
    }