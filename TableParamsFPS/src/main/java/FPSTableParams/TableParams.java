package FPSTableParams;
import FPSModels.FPSModel;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.List;

public class TableParams extends AnchorPane implements Initializable {
    @FXML public TableColumn<FPSModel, Integer> powerColumn;
    @FXML public TableColumn<FPSModel, Byte> ksvColumn;
    @FXML public TableColumn<FPSModel, Boolean> supplyColumn;
    @FXML public TableColumn<FPSModel, Boolean> radiationColumn;
    @FXML public TableColumn<FPSModel, Boolean> filterColumn;
    @FXML public TableColumn<FPSModel, Byte> temperatureColumn;
    @FXML public TableColumn<FPSModel, Boolean> controlColumn;
    @FXML public TableView<FPSModel> tableStatus;

    public List<FPSModel> listParamFPS = new ArrayList();

public TableParams() {
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TableParams.fxml"));
    fxmlLoader.setRoot(this);
    fxmlLoader.setController(this);

    try {

        fxmlLoader.load();

    }
    catch (IOException exception) {

        throw new RuntimeException(exception);
    }
}

    public void UpdateStatus(List<FPSModel> list) {
        try {
            if (list == null) {
                return;
            }
            ObservableList<FPSModel> tempList = FXCollections.observableArrayList(list);
               this.tableStatus.setItems(tempList);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.tableStatus.setPlaceholder(new Label("Записи в таблице отсутствуют!"));
        // определение полей для конкретного столбца в таблице
        this.powerColumn.setCellValueFactory(new PropertyValueFactory("power"));
        this.ksvColumn.setCellValueFactory(new PropertyValueFactory("ksv"));
        this.supplyColumn.setCellValueFactory(new PropertyValueFactory("supply"));
        this.radiationColumn.setCellValueFactory(new PropertyValueFactory("radiation"));
        this.filterColumn.setCellValueFactory(new PropertyValueFactory("filter"));
        this.temperatureColumn.setCellValueFactory(new PropertyValueFactory("temperature"));
        this.controlColumn.setCellValueFactory(new PropertyValueFactory("control"));

        //конвертация из boolean в string

        this.supplyColumn.setCellFactory(new Callback<TableColumn<FPSModel, Boolean>, TableCell<FPSModel, Boolean>>() {
            public TableCell<FPSModel, Boolean> call(TableColumn<FPSModel, Boolean> param) {
                return new TableCell<FPSModel, Boolean>() {
                    private final Label label = new Label();

                    {
                        this.label.setStyle("-fx-text-fill: white");
                    }

                    protected void updateItem(Boolean value, boolean empty) {
                        super.updateItem(value, empty);
                        if (empty) {
                            this.setGraphic((Node)null);
                        } else {
                            if (value) {
                                this.label.setText("ВКЛ.");
                            } else {
                                this.label.setText("ОТКЛ");
                            }

                            this.setGraphic(this.label);
                        }

                    }
                };
            }
        });

        //конвертация из boolean в string

        this.radiationColumn.setCellFactory(new Callback<TableColumn<FPSModel, Boolean>, TableCell<FPSModel, Boolean>>() {
            public TableCell<FPSModel, Boolean> call(TableColumn<FPSModel, Boolean> param) {
                return new TableCell<FPSModel, Boolean>() {
                    private final Label label = new Label();

                    {
                        this.label.setStyle("-fx-text-fill: white");
                    }

                    protected void updateItem(Boolean value, boolean empty) {
                        super.updateItem(value, empty);
                        if (empty) {
                            this.setGraphic((Node)null);
                        } else {
                            if (value) {
                                this.label.setText("ВКЛ.");
                            } else {
                                this.label.setText("ОТКЛ");
                            }

                            this.setGraphic(this.label);
                        }

                    }
                };
            }
        });


        //конвертация из boolean в string
        this.filterColumn.setCellFactory(new Callback<TableColumn<FPSModel, Boolean>, TableCell<FPSModel, Boolean>>() {
            public TableCell<FPSModel, Boolean> call(TableColumn<FPSModel, Boolean> param) {
                return new TableCell<FPSModel, Boolean>() {
                    private final Label label = new Label();

                    {
                        this.label.setStyle("-fx-text-fill: white");
                    }

                    protected void updateItem(Boolean value, boolean empty) {
                        super.updateItem(value, empty);
                        if (empty) {
                            this.setGraphic((Node)null);
                        } else {
                            if (value) {
                                this.label.setText("ВКЛ.");
                            } else {
                                this.label.setText("ОТКЛ");
                            }

                            this.setGraphic(this.label);
                        }

                    }
                };
            }
        });
        //конвертация из boolean в string

        this.controlColumn.setCellFactory(new Callback<TableColumn<FPSModel, Boolean>, TableCell<FPSModel, Boolean>>() {
            public TableCell<FPSModel, Boolean> call(TableColumn<FPSModel, Boolean> param) {
                return new TableCell<FPSModel, Boolean>() {
                    private final Label label = new Label();

                    {
                        this.label.setStyle("-fx-text-fill: white");
                    }

                    protected void updateItem(Boolean value, boolean empty) {
                        super.updateItem(value, empty);
                        if (empty) {
                            this.setGraphic((Node)null);
                        } else {
                            if (value) {
                                this.label.setText("ДИСТ.");
                            } else {
                                this.label.setText("МЕСТН.");
                            }

                            this.setGraphic(this.label);
                        }

                    }
                };
            }
        });

    }
}
